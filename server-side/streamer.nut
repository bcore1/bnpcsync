class BNpcStreamer {
    _buckets = null

    constructor() {
        this._buckets = {}
    }

    function _key(x, z) {
        return (x / 1000).tointeger() + "," + (z / 1000).tointeger()
    }

    function query(x, z, size = 0) {
        local result = []

        for (local x = -size; x <= size; ++x)
        for (local z = -size; z <= size; ++z) {
            local key = this._key(x, z)

            if (key in this._buckets) {
                result.extend(this._buckets[key])
            }
        }

        return result
    }

    function insert(element, x, z) {
        local key = this._key(x, z)
        local buckets = null

        if (!(key in this._buckets)) {
            buckets = []
            this._buckets[key] <- buckets
        }

        buckets.push(element)
    }

    function remove(element, x, z) {
        local key = this._key(x, z)

        if (key in this._buckets) {
            local bucket = this._buckets[key]

            foreach (i, bucket_element in bucket) {
                if (element == bucket_element) {
                    bucket[i] = bucket[bucket.len() - 1]
                    bucket.pop()

                    return
                }
            }
        }
    }
}

local max_slots = getMaxSlots()
local registry = BNpcRegistry

// getMaxBots()

local function _offset(id) {
    return id - max_slots
}

// Implementacja OOP + functions ;)

///////////////////////////////////////////
/// Hooks
///////////////////////////////////////////

local original_spawnPlayer = spawnPlayer
local original_unspawnPlayer = unspawnPlayer
local original_setPlayerInstance = setPlayerInstance
local original_getPlayerInstance = getPlayerInstance
local original_setPlayerPosition = setPlayerPosition
local original_getPlayerPosition = getPlayerPosition
local original_setPlayerAngle = setPlayerAngle
local original_getPlayerAngle = getPlayerAngle
local original_setPlayerWorld = setPlayerWorld
local original_getPlayerWorld = getPlayerWorld

///////////////////////////////////////////
/// Functions
///////////////////////////////////////////

function createNpc(name) {
    return _offset(registry.create(name))
}

//-----------------------------------------

function destroyNpc(pid) {
    local npc = registry.get(_offset(pid))
    if (npc.spawned) {
        unspawnPlayer(pid)
    }

    return registry.destroy(_offset(pid))
}

//-----------------------------------------

function hitPlayer(kid, pid) {
    // TODO
}

//-----------------------------------------

function spawnPlayer(pid) {
    if (pid < max_slots) {
        return original_spawnPlayer(pid)
    }

    local npc = registry.get(_offset(pid))
    if (npc) {
        // Setup only a flag?
        local packet = NpcSpawnMessage(pid).toPacket()

        // Send to all players within range
        // send to other players
        packet.send(pid, RELIABLE_ORDERED)
    }
}

//-----------------------------------------

function unspawnPlayer(pid) {
    if (pid < max_slots) {
        original_unspawnPlayer(pid)
        return
    }

    local npc = registry.get(_offset(pid))
    if (npc) {
        local packet = NpcSpawnMessage(pid).toPacket()
        // send to other players
        packet.send(pid, RELIABLE_ORDERED)
    }
}

//-----------------------------------------

function setPlayerInstance(pid, instance) {
    original_setPlayerInstance(pid, instance)
}

//-----------------------------------------

function getPlayerInstance(pid) {
    return original_getPlayerInstance(pid)
}

//-----------------------------------------

function setPlayerPosition(pid, x, y, z) {
    original_setPlayerPosition(pid, x, y, z)
}

//-----------------------------------------

function getPlayerPosition(pid) {
    return original_getPlayerPosition(pid)
}

//-----------------------------------------

function setPlayerAngle(pid, angle) {
    original_setPlayerAngle(pid, angle)
}

//-----------------------------------------

function getPlayerAngle(pid) {
    return original_getPlayerAngle(pid)
}

//-----------------------------------------

function setPlayerWorld(pid, world) {
    original_setPlayerWorld(pid, world)
}

//-----------------------------------------

function getPlayerWorld(pid) {
    return original_getPlayerWorld(pid)
}

/*
int getPlayerPing(int pid)
void kick(int pid, string reason)
void ban(int pid, int minutes, string reason)
int getPlayerAniId(int pid)
int getPlayerVirtualWorld(int pid)
void setPlayerVirtualWorld(int pid, int virtualWorld)
string getPlayerIP(int pid)
string getPlayerSerial(int pid)
string getPlayerMacAddr(int pid)
void playAniId(int pid, int id)
void stopAni(int pid)
int getAniId(int pid)
int getPlayerArmor(int pid)
int getPlayerMeleeWeapon(int pid)
int getPlayerRangedWeapon(int pid)
int getPlayerHelmet(int pid)
int getPlayerShield(int pid)
void setPlayerMana(int pid, int value)
void setPlayerMaxMana(int pid, int value)
int getPlayerMana(int id)
int getPlayerMaxMana(int id)
void setPlayerMagicLevel(int pid, int value[1-6])
int getPlayerMagicLevel(int pid)
void setPlayerRespawnTime(int pid, int time) // int time>1000
int getPlayerRespawnTime(int pid)
bool isPlayerConnected(int id)
bool isPlayerSpawned(int id)
bool isPlayerDead(int id)
bool isPlayerUnconscious(int id)
int getPlayerFocus(int id)
void setPlayerName(int id, string name)
string getPlayerName(int id)
void setPlayerColor(int id, int r, int g, int b)
table {r, g, b} getPlayerColor(int id)
void setPlayerHealth(int id, int hp)
int getPlayerHealth(int id)
void setPlayerMaxHealth(int id, int maxHp)
int getPlayerMaxHealth(int id)
void setPlayerWeaponMode(int id, int wm)
int getPlayerWeaponMode(int id)
void setPlayerStrength(int id, int str)
int getPlayerStrength(int id)
void setPlayerDexterity(int id, int dex)
int getPlayerDexterity(int id)
void setPlayerSkillWeapon(int id, int skill_id, int value)
int getPlayerSkillWeapon(int id)
void setPlayerTalent(int id, int talent_id, int value)
int getPlayerTalent(int id, int talent_id)
void setPlayerVisual(int pid, string bodyModel, int bodyTxt, string headModel, int headTxt)
table {bodyModel, bodyTxt, headModel, headTxt} getPlayerVisual(int pid)
void setPlayerScale(float x, float y, float x)
table {x, y, z} getPlayerScale(int id)
void setPlayerFatness(int id, float fatness)
float getPlayerFatness(int id)
*/
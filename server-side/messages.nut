local SYNC_PACKET_HEADER = 255

//-----------------------------------------

enum NpcSyncPacket {
    SPAWN,
    UNSPAWN
}

///////////////////////////////////////////
/// Messages
///////////////////////////////////////////

class BaseNpcSyncMessage {
    id = null
    type = null

    constructor(pid) {
        this.id = pid
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(SYNC_PACKET_HEADER)
        packet.writeUInt8(this.type)
        packet.writeUInt16(this.id)
    }

    static function fromPacket(packet) {
        return this()
    }
}

//-----------------------------------------

class NpcSpawnMessage extends BaseNpcSyncMessage {
    type = NpcSyncPacket.SPAWN
}

//-----------------------------------------

class NpcUnspawnMessage extends BaseNpcSyncMessage {
    type = NpcSyncPacket.UNSPAWN
}

//-----------------------------------------

class NpcUnspawnMessage extends BaseNpcSyncMessage {
    type = NpcSyncPacket.UNSPAWN
}
class BNpcData {
    spawned = false
    name = null
    instance = "PC_HERO"
    health = 40
    max_health = 40
    mana = 10
    max_mana = 10
    strength = 10
    dexterity = 10

    constructor(name) {
        this.name = name
    }
}

//-----------------------------------------

class BNpcRegistry {
    static npcs = []

    function _nextId() {
        foreach (i, npc in this.npcs) {
            if (npc == null) {
                return i
            }
        }
        
        return -1
    }

    function get(id) {
        return this.npcs[id]
    }

    function create(name) {
        local npc = BNpcData(name)
        local id = this._nextId()

        if (id == -1) {
            this.npcs.push(npc)
        } else {
            this.npcs[id] = npc
        }
        
        return id
    }

    function destroy(id) {
        local npc = npcs[id]
        if (npc == null) {
            return false
        }

        npcs[pid] = null
        return true
    }
}

// For internal usage
addEvent("BNpcSync:onPacket")

///////////////////////////////////////////
/// Events
///////////////////////////////////////////

local function get_npc_message(packet) {
    local header = packet.readUInt8()
    if (header != NPC_SYNC_PACKET_HEADER) {
        return null
    }

    return NpcSyncMessage.fromPacket(packet)
}

//-----------------------------------------

local function packet_data_client(packet) {
    local message = get_npc_message(packet)
    if (message) {
        callEvent("BNpcSync:onPacket", packet, message)
    }
}

//-----------------------------------------

local function packet_data_server(pid, packet) {
    local message = get_npc_message(packet)
    if (message) {
        callEvent("BNpcSync:onPacket", pid, packet, message)
    }
}

// Register
addEventHandler("onPacket", CLIENT_SIDE ? packet_data_client : packet_data_server)
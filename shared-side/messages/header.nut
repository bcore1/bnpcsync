const NPC_SYNC_PACKET_HEADER = 255

//-----------------------------------------

enum NpcSyncPacket {
    SPAWN,
    UNSPAWN,
    POSITION
}

///////////////////////////////////////////
/// Base message
///////////////////////////////////////////

class NpcSyncMessage {
    id = null
    type = null

    constructor(pid) {
        this.id = pid
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(SYNC_PACKET_HEADER)
        packet.writeUInt8(this.type)
        packet.writeUInt16(this.id)
    }

    static function fromPacket(packet) {
        try {
            local message = this(-1)
            message.type = packet.readUInt8()
            message.id = packet.readUInt16()

            return message

        } catch (e) {
            return null
        }
    }
}

//-----------------------------------------

class NpcUnspawnMessage extends NpcSyncMessage {
    type = NpcSyncPacket.UNSPAWN
}
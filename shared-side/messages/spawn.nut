class NpcSpawnMessage extends NpcSyncMessage {
    type = NpcSyncPacket.SPAWN

    instance = null
    x = 0
    y = 0
    z = 0
    angle = 0

    function toPacket() {
        local packet = base.toPacket()
        
        packet.writeString(this.instance)
        packet.writeFloat(this.x)
        packet.writeFloat(this.y)
        packet.writeFloat(this.z)
        packet.writeUInt16(this.angle)

        return packet
    }

    static function fromPacket(header, packet) {
        try {
            local message = this(header.id)
            message.instance = packet.readString()
            message.x = packet.readFloat()
            message.y = packet.readFloat()
            message.z = packet.readFloat()
            message.angle = packet.readUInt16()

            return message

        } catch (e) {
            return null
        }
    }
}

//-----------------------------------------

class NpcUnspawnMessage extends NpcSyncMessage {
    type = NpcSyncPacket.UNSPAWN
}

///////////////////////////////////////////
/// Packet handler
///////////////////////////////////////////

if (CLIENT_SIDE) {
    addEvent("BNpcSync:onSpawn")
    addEvent("BNpcSync:onUnspawn")

    local function packet_data(packet, header) {
        if (header.type == NpcSyncPacket.SPAWN) {
            local message = NpcSpawnMessage.fromPacket(header)
            if (message) {
                callEvent("BNpcSync:onSpawn", message)
            }

        } else if (header.type == NpcSyncPacket.UNSPAWN) {
            local message = NpcUnspawnMessage(header)
            callEvent("BNpcSync:onUnspawn", message)
        }
    }

    addEventHandler("BNpcSync:onPacket", packet_data)
}

class NpcPositionMessage extends NpcSyncMessage {
    type = NpcSyncPacket.POSITION

    x = 0
    y = 0
    z = 0

    function toPacket() {
        local packet = base.toPacket()
        
        packet.writeFloat(this.x)
        packet.writeFloat(this.y)
        packet.writeFloat(this.z)

        return packet
    }

    static function fromPacket(header, packet) {
        try {
            local message = this(header.id)
            message.x = packet.readFloat()
            message.y = packet.readFloat()
            message.z = packet.readFloat()

            return message

        } catch (e) {
            return null
        }
    }
}

///////////////////////////////////////////
/// Packet handler
///////////////////////////////////////////

if (CLIENT_SIDE) {
    addEvent("BNpcSync:onPosition")

    local function packet_data(packet, header) {
        if (header.type != NpcSyncPacket.POSITION) {
            return
        }

        local message = NpcPositionMessage.fromPacket(header, packet)
        if (message) {
            callEvent("BNpcSync:onPosition", message)
        }
    }

    addEventHandler("BNpcSync:onPacket", packet_data)
}
